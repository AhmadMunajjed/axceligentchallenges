﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public class Humanoid : ISkill
    {
        ISkill skill;
        public Humanoid(ISkill skill)
        {
            this.skill = skill;
        }

        public Humanoid()
        {

        }

        public string ShowSkill()
        {
            if (skill != null)
            {
                return skill.ShowSkill();
            }
            return "no skill is defined";
        }

    }
}
