﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public interface ISkill
    {
        string ShowSkill();
    }
}
