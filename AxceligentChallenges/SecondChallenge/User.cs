﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public class User
    {
        static List<User> data = new List<User>();

        public User()
        {

        }

        public User(string name)
        {
            Name = name;
        }
        
        public string Name { get; set; }

        public int GetUsersCount()
        {
            return data.Count;
        }

        public void Add(string name)
        {
            data.Add(new User(name));
        }
    }
}
