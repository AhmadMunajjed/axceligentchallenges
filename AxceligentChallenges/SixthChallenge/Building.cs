﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public class Building
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Building Next { get; set; }
        public Building Prev { get; set; }

        public Building()
        {

        }

        public Building(string name, Building prev)
        {
            Name = name;
            Prev = prev;
        }

        private Building AddNext(string name)
        {
            Next = new Building(name, this);
            return Next;
        }

        public Building AddKitchen()
        {
            return AddNext("kitchen");
        }

        public Building AddBedroom(string name)
        {
            return AddNext(name + " room");
        }

        public Building AddBalcony()
        {
            return AddNext("balcony");
        }

        public void Build()
        {
            Building pointer = this;

            // go back to the first node in order to print the building from the beginning 
            while (pointer.Prev != null)
            {
                pointer = pointer.Prev;
            }

            // the first block which added after the constructor
            pointer = pointer.Next;

            Description = pointer.Name;

            // move next to build the Description
            while (pointer.Next != null)
            {
                Description += ", " + pointer.Next.Name;
                pointer = pointer.Next;
            }
        }

        public string Describe()
        {
            return Description;
        }

    }
}
