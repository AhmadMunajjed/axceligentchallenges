﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public class Alexa
    {
        AlexaConfiguration configuration;
        public Alexa()
        {
            configuration = new AlexaConfiguration()
            {
                GreetingMessage = "hello, i am Alexa"
            };
        }
        
        public string Talk()
        {
            return configuration.GreetingMessage;
        }

        public void Configure(Action<AlexaConfiguration> actionConfigs)
        {
            actionConfigs(configuration);
        }
    }

    public class AlexaConfiguration
    {
        private string greetingMessage;
        public string GreetingMessage
        {
            get
            {
                return GetMessage();
            }

            set => greetingMessage = value;
        }

        public string OwnerName { get; set; }

        public string GetMessage()
        {
            var properties = typeof(AlexaConfiguration).GetProperties();
            foreach (var item in properties)
            {
                var propertyName = item.Name;
                // to avoid throwing StackOverflowException
                if (propertyName == "GreetingMessage") continue;
                // to get the value of this field
                string result = (string)GetType().GetProperty(propertyName).GetValue(this);
                // to replace the name with the value 
                greetingMessage = greetingMessage.Replace($"{{{propertyName}}}", result);
            }

            return greetingMessage;
        }
    }
}
