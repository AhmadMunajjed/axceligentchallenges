﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    public class SumofBiggestNeighbor
    {
        #region variables
        int[] _arrayItems;
        Dictionary<int, int> _itemsRepeating = new Dictionary<int, int>();
        int _maximumRepeatingTimes = 0;
        int _minimumAllowedRepeatingTimes
        {
            get { return _maximumRepeatingTimes - 1; }
        }
        #endregion

        public int Challenge(int[] input)
        {
            _arrayItems = input;

            PopulateItemsRepeating();

            PopulateMaxTimeOfRepeating();

            PopulateAllowedElementsOnly();
            
            return getMaxSumOfTwoCombinationNumbers();
        }

        public void PopulateItemsRepeating()
        {
            foreach (int item in _arrayItems)
            {
                if (_itemsRepeating.ContainsKey(item))
                    _itemsRepeating[item]++;
                else
                    _itemsRepeating[item] = 1;
            }
        }

        private int[] GetAllowedFigures()
        {
            return _itemsRepeating.Where(e => e.Value >= _minimumAllowedRepeatingTimes).Select(e => e.Key).ToArray();
        }

        private void PopulateAllowedElementsOnly()
        {
            _arrayItems = _arrayItems.Where(e => GetAllowedFigures().Contains(e)).ToArray();
        }

        private void PopulateMaxTimeOfRepeating()
        {
            _maximumRepeatingTimes = _itemsRepeating.Max(e => e.Value);
        }

        private int getRepeatingTimes(int number)
        {
            return _itemsRepeating[number];
        }

        private int getMaxSumOfTwoCombinationNumbers()
        {
            int maxCombination = 0;
            for (int i = 0; i < _arrayItems.Length; i++)
            {
                if (i + 1 <= _arrayItems.Length - 1)
                {
                    var temp = _arrayItems[i] + _arrayItems[i + 1];
                    if (temp > maxCombination)
                    {
                        maxCombination = temp;
                    }
                }
            }
            return maxCombination;
        }
    }
}
