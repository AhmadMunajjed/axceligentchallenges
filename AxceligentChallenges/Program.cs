﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxceligentChallenges
{
    class Program
    {
        /* the first challenge, and the time complexity is O(n) */
        /*
        static void Main(string[] args)
        {
            int[] sumOfBiggestNeighborInput = { 1, 6, 1, 2, 6, 1, 6, 6 };
            //  { 1, 2, 1, 5, 5, 3, 3, 3, 4 }; //

            var sumofBiggestNeighbor = new SumofBiggestNeighbor();
            var result = sumofBiggestNeighbor.Challenge(sumOfBiggestNeighborInput);
            
            Console.WriteLine(result);
        }
        */

        /* the second challenge */
        /*
        private static void Main(string[] args)
        {
            while (true)
            {
                var user = new User();
                Console.WriteLine("please enter the username, or q to exit");
                var userName = Console.ReadLine();
                if (userName == "q")
                {
                    break;
                }

                user.Add(userName);
                Console.WriteLine($"number of addedUser {user.GetUsersCount()}");
            }
        }
        */

        /* the third challenge */
        /*
        private static void Main(string[] args)
        {
            var john = new Humanoid(new Dancing());
            Console.WriteLine(john.ShowSkill()); //print dancing

            var alex = new Humanoid(new Cooking());
            Console.WriteLine(alex.ShowSkill());//print cooking

            var bob = new Humanoid();
            Console.WriteLine(bob.ShowSkill());//print no skill is defined

        }
        */

        /* the fourth challenge */
        /*
        private static void Main(string[] args)
        {
            var alexa = new Alexa();
            Console.WriteLine(alexa.Talk()); //print hello, i am Alexa

            alexa.Configure(x =>
            {
                x.GreetingMessage = "Hello {OwnerName}, I'm at your service";
                x.OwnerName = "Bob Marley";
            });

            Console.WriteLine(alexa.Talk()); //print Hello Bob Marley, I'm at your service

            Console.WriteLine("press any key to exit ...");
            Console.ReadKey();

        }
        */

        /* the fifth challenge */
        /*
         the function should look like “SomeCalculation” should look like:  
         async Task<Tuple<double, int>> SomeCalculation(long x, long y, bool condition);
        */

        /* the sixth challenge */
        /**/
        private static void Main(string[] args)
        {
            var myHouse = new Building()
                .AddKitchen()
                .AddBedroom("master")
                .AddBedroom("guest")
                .AddBalcony();

            myHouse.Build();

            //kitchen, master room, guest room, balcony
            Console.WriteLine(myHouse.Describe());

            myHouse.AddKitchen().AddBedroom("another");

            //before build the house description still is as before
            //kitchen, master room, guest room, balcony
            Console.WriteLine(myHouse.Describe());
            myHouse.Build();

            //it only shows the kitchen after build
            //kitchen, master room, guest room, balcony, kitchen, another room
            Console.WriteLine(myHouse.Describe());
        }
        
    }
}
